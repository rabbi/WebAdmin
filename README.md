#WebAdmin

开发目标：
    1、技术试验田
    2、通用模块

采用技术：
    Spring MVC
    Spring Security
    Mybatis
    Activiti
    Apache-Common
    Druid


# 0.1
系统管理搞完，不过好像有不少Bug，先不管啦，开始做基于配置的通用列表，不然每个每个拷贝文件
的做法太不爽了，下一个版本发版的目标：
    
    1、通用简单列表
    2、通用树
    3、修复权限管理的Bug