<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ tag language="java" pageEncoding="UTF-8" %>
<%@ attribute name="id" required="true" %>
<a href="deleteEntity?id=${id}&limit=${empty param.limit?0:param.limit}&offset=${empty param.offset?0:param.offset}"
   class="btn btn-danger ">删除</a>
