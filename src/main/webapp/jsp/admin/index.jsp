<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="header.jsp" %>

<script src="${pageContext.servletContext.contextPath}/static/hightcharts/js/highcharts.js"></script>
<section class="content-header">
    <h1>
        仪表板
    </h1>
</section>
<section class="content">
    <ul id="myDashboard">

    </ul>
    <%--<div id="container2"></div>--%>
    <%--<div class="row" style="margin-top: 20px">--%>
    <%--<div id="container" class="col-lg-6"></div>--%>
    <%--<div id="container1" class="col-lg-6"></div>--%>
    <%--</div>--%>

</section>
<div id="cpuinfo">
    <table>
        <tr>
            <td style="width: 40%">CPU核心数</td>
            <td>${availableProcessors}</td>
        </tr>
    </table>
</div>
<div id="cpuinfo1">
    <table>
        <tr>
            <td style="width: 40%">CPU核心数</td>
            <td>${availableProcessors}</td>
        </tr>
    </table>
</div>
<script>
    var widgetDefinitions = [
        {
            widgetTitle: "CPU信息",
            widgetId: "wg_cpu",
            widgetContent: $('#cpuinfo'),
        },
        {
            widgetTitle: "CPU信息",
            widgetId: "wg_cpu1",
            widgetContent: $('#cpuinfo1'),
        },
    ]

    $("#myDashboard").sDashboard({
        dashboardData: widgetDefinitions
    });

</script>
<%--<script>--%>
<%--$(function () {--%>
<%--$('#container1').highcharts({--%>
<%--chart: {--%>
<%--type: 'spline'--%>
<%--},--%>
<%--title: {--%>
<%--text: 'Wind speed during two days'--%>
<%--},--%>
<%--subtitle: {--%>
<%--text: 'October 6th and 7th 2009 at two locations in Vik i Sogn, Norway'--%>
<%--},--%>
<%--xAxis: {--%>
<%--type: 'datetime'--%>
<%--},--%>
<%--yAxis: {--%>
<%--title: {--%>
<%--text: 'Wind speed (m/s)'--%>
<%--},--%>
<%--min: 0,--%>
<%--minorGridLineWidth: 0,--%>
<%--gridLineWidth: 0,--%>
<%--alternateGridColor: null,--%>
<%--plotBands: [{ // Light air--%>
<%--from: 0.3,--%>
<%--to: 1.5,--%>
<%--color: 'rgba(68, 170, 213, 0.1)',--%>
<%--label: {--%>
<%--text: 'Light air',--%>
<%--style: {--%>
<%--color: '#606060'--%>
<%--}--%>
<%--}--%>
<%--}, { // Light breeze--%>
<%--from: 1.5,--%>
<%--to: 3.3,--%>
<%--color: 'rgba(0, 0, 0, 0)',--%>
<%--label: {--%>
<%--text: 'Light breeze',--%>
<%--style: {--%>
<%--color: '#606060'--%>
<%--}--%>
<%--}--%>
<%--}, { // Gentle breeze--%>
<%--from: 3.3,--%>
<%--to: 5.5,--%>
<%--color: 'rgba(68, 170, 213, 0.1)',--%>
<%--label: {--%>
<%--text: 'Gentle breeze',--%>
<%--style: {--%>
<%--color: '#606060'--%>
<%--}--%>
<%--}--%>
<%--}, { // Moderate breeze--%>
<%--from: 5.5,--%>
<%--to: 8,--%>
<%--color: 'rgba(0, 0, 0, 0)',--%>
<%--label: {--%>
<%--text: 'Moderate breeze',--%>
<%--style: {--%>
<%--color: '#606060'--%>
<%--}--%>
<%--}--%>
<%--}, { // Fresh breeze--%>
<%--from: 8,--%>
<%--to: 11,--%>
<%--color: 'rgba(68, 170, 213, 0.1)',--%>
<%--label: {--%>
<%--text: 'Fresh breeze',--%>
<%--style: {--%>
<%--color: '#606060'--%>
<%--}--%>
<%--}--%>
<%--}, { // Strong breeze--%>
<%--from: 11,--%>
<%--to: 14,--%>
<%--color: 'rgba(0, 0, 0, 0)',--%>
<%--label: {--%>
<%--text: 'Strong breeze',--%>
<%--style: {--%>
<%--color: '#606060'--%>
<%--}--%>
<%--}--%>
<%--}, { // High wind--%>
<%--from: 14,--%>
<%--to: 15,--%>
<%--color: 'rgba(68, 170, 213, 0.1)',--%>
<%--label: {--%>
<%--text: 'High wind',--%>
<%--style: {--%>
<%--color: '#606060'--%>
<%--}--%>
<%--}--%>
<%--}]--%>
<%--},--%>
<%--tooltip: {--%>
<%--valueSuffix: ' m/s'--%>
<%--},--%>
<%--plotOptions: {--%>
<%--spline: {--%>
<%--lineWidth: 4,--%>
<%--states: {--%>
<%--hover: {--%>
<%--lineWidth: 5--%>
<%--}--%>
<%--},--%>
<%--marker: {--%>
<%--enabled: false--%>
<%--},--%>
<%--pointInterval: 3600000, // one hour--%>
<%--pointStart: Date.UTC(2009, 9, 6, 0, 0, 0)--%>
<%--}--%>
<%--},--%>
<%--series: [{--%>
<%--name: 'Hestavollane',--%>
<%--data: [4.3, 5.1, 4.3, 5.2, 5.4, 4.7, 3.5, 4.1, 5.6, 7.4, 6.9, 7.1,--%>
<%--7.9, 7.9, 7.5, 6.7, 7.7, 7.7, 7.4, 7.0, 7.1, 5.8, 5.9, 7.4,--%>
<%--8.2, 8.5, 9.4, 8.1, 10.9, 10.4, 10.9, 12.4, 12.1, 9.5, 7.5,--%>
<%--7.1, 7.5, 8.1, 6.8, 3.4, 2.1, 1.9, 2.8, 2.9, 1.3, 4.4, 4.2,--%>
<%--3.0, 3.0]--%>

<%--}, {--%>
<%--name: 'Voll',--%>
<%--data: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.1, 0.0, 0.3, 0.0,--%>
<%--0.0, 0.4, 0.0, 0.1, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,--%>
<%--0.0, 0.6, 1.2, 1.7, 0.7, 2.9, 4.1, 2.6, 3.7, 3.9, 1.7, 2.3,--%>
<%--3.0, 3.3, 4.8, 5.0, 4.8, 5.0, 3.2, 2.0, 0.9, 0.4, 0.3, 0.5, 0.4]--%>
<%--}]--%>
<%--,--%>
<%--navigation: {--%>
<%--menuItemStyle: {--%>
<%--fontSize: '10px'--%>
<%--}--%>
<%--}--%>
<%--});--%>

<%--$('#container2').highcharts({--%>
<%--chart: {},--%>
<%--title: {--%>
<%--text: 'Combination chart'--%>
<%--},--%>
<%--xAxis: {--%>
<%--categories: ['Apples', 'Oranges', 'Pears', 'Bananas', 'Plums']--%>
<%--},--%>
<%--tooltip: {--%>
<%--formatter: function () {--%>
<%--var s;--%>
<%--if (this.point.name) { // the pie chart--%>
<%--s = '' +--%>
<%--this.point.name + ': ' + this.y + ' fruits';--%>
<%--} else {--%>
<%--s = '' +--%>
<%--this.x + ': ' + this.y;--%>
<%--}--%>
<%--return s;--%>
<%--}--%>
<%--},--%>
<%--labels: {--%>
<%--items: [{--%>
<%--html: 'Total fruit consumption',--%>
<%--style: {--%>
<%--left: '40px',--%>
<%--top: '8px',--%>
<%--color: 'black'--%>
<%--}--%>
<%--}]--%>
<%--},--%>
<%--series: [{--%>
<%--type: 'column',--%>
<%--name: 'Jane',--%>
<%--data: [3, 2, 1, 3, 4]--%>
<%--}, {--%>
<%--type: 'column',--%>
<%--name: 'John',--%>
<%--data: [2, 3, 5, 7, 6]--%>
<%--}, {--%>
<%--type: 'column',--%>
<%--name: 'Joe',--%>
<%--data: [4, 3, 3, 9, 0]--%>
<%--}, {--%>
<%--type: 'spline',--%>
<%--name: 'Average',--%>
<%--data: [3, 2.67, 3, 6.33, 3.33],--%>
<%--marker: {--%>
<%--lineWidth: 2,--%>
<%--lineColor: Highcharts.getOptions().colors[3],--%>
<%--fillColor: 'white'--%>
<%--}--%>
<%--}, {--%>
<%--type: 'pie',--%>
<%--name: 'Total consumption',--%>
<%--data: [{--%>
<%--name: 'Jane',--%>
<%--y: 13,--%>
<%--color: Highcharts.getOptions().colors[0] // Jane's color--%>
<%--}, {--%>
<%--name: 'John',--%>
<%--y: 23,--%>
<%--color: Highcharts.getOptions().colors[1] // John's color--%>
<%--}, {--%>
<%--name: 'Joe',--%>
<%--y: 19,--%>
<%--color: Highcharts.getOptions().colors[2] // Joe's color--%>
<%--}],--%>
<%--center: [100, 80],--%>
<%--size: 100,--%>
<%--showInLegend: false,--%>
<%--dataLabels: {--%>
<%--enabled: false--%>
<%--}--%>
<%--}]--%>
<%--});--%>

<%--$('#container').highcharts({--%>
<%--chart: {--%>
<%--plotBackgroundColor: null,--%>
<%--plotBorderWidth: null,--%>
<%--plotShadow: false--%>
<%--},--%>
<%--title: {--%>
<%--text: 'Browser market shares at a specific website, 2010'--%>
<%--},--%>
<%--tooltip: {--%>
<%--pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'--%>
<%--},--%>
<%--plotOptions: {--%>
<%--pie: {--%>
<%--allowPointSelect: true,--%>
<%--cursor: 'pointer',--%>
<%--dataLabels: {--%>
<%--enabled: true,--%>
<%--color: '#000000',--%>
<%--connectorColor: '#000000',--%>
<%--format: '<b>{point.name}</b>: {point.percentage:.1f} %'--%>
<%--}--%>
<%--}--%>
<%--},--%>
<%--series: [{--%>
<%--type: 'pie',--%>
<%--name: 'Browser share',--%>
<%--data: [--%>
<%--['Firefox', 45.0],--%>
<%--['IE', 26.8],--%>
<%--{--%>
<%--name: 'Chrome',--%>
<%--y: 12.8,--%>
<%--sliced: true,--%>
<%--selected: true--%>
<%--},--%>
<%--['Safari', 8.5],--%>
<%--['Opera', 6.2],--%>
<%--['Others', 0.7]--%>
<%--]--%>
<%--}]--%>
<%--});--%>
<%--});--%>
<%--</script>--%>
<!-- /.right-side -->
<%@include file="footer.jsp" %>

