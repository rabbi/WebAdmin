<%@ page language="java" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="app" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<form:hidden path="id"></form:hidden>
<app:s_input labelName="角色名称" path="name"></app:s_input>
<app:s_input labelName="角色备注" path="comment"></app:s_input>

