<%@ page language="java" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="app" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<form:hidden path="id"></form:hidden>
<app:s_input labelName="字典分组" path="dictgroup"></app:s_input>
<app:s_input labelName="字典名称" path="dictkey"></app:s_input>
<app:s_input labelName="字典值" path="dictvalues"></app:s_input>

