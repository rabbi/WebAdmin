<%@ page language="java" pageEncoding="utf-8" %>
<%@ include file="../../header.jsp" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="app" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<section class="content">
    <app:t_message></app:t_message>

    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">字典管理</h3>
        </div>
        <form class="form-horizontal">
            <div class="box-body">
                <app:searchinput defaultValue="${param.dictgroup}" labelName="字典分组"
                                 inputID="dictgroup"></app:searchinput>
                <app:searchinput defaultValue="${param.name}" labelName="字典名称" inputID="dictkey"></app:searchinput>
            </div>
            <div class="box-footer">
                <sec:authorize access="hasAuthority('ROLE_SYS_DICT_ADD')">
                    <app:addButton></app:addButton>
                </sec:authorize>
                <app:searchButton></app:searchButton>
            </div>
        </form>
    </div>
    <div class="box">
        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
            <table class="table table-hover">
                <tbody>
                <tr>
                    <th style="width: 10%;">字典分组</th>
                    <th style="width: 10%;">字典名称</th>
                    <th style="width: 10%;">字典值</th>
                    <th style="width: 10%;">创建人</th>
                    <th style="width: 10%;">创建时间</th>
                    <th style="width: 10%;">更新人</th>
                    <th style="width: 10%;">更新时间</th>
                    <th style="width: 15%;">操作</th>
                </tr>
                <c:forEach var="vo" items="${result}">
                    <tr>
                        <td>${vo.dictgroup}</td>
                        <td>${vo.dictkey}</td>
                        <td>${vo.dictvalues}</td>
                        <td>${vo.creator}</td>
                        <td>${vo.createtime}</td>
                        <td>${vo.updator}</td>
                        <td>${vo.updatetime}</td>
                        <td>
                            <sec:authorize access="hasAuthority('ROLE_SYS_DICT_EDIT')">
                                <app:editButton id="${vo.id}"></app:editButton>
                            </sec:authorize>
                            <sec:authorize access="hasAuthority('ROLE_SYS_DICT_DELETE')">
                                <app:deleteButton id="${vo.id}"></app:deleteButton>
                            </sec:authorize>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>

            </table>
        </div>
        <%@include file="../../common/pagenation.jsp" %>
    </div>

</section>

<%@ include file="../../footer.jsp" %>