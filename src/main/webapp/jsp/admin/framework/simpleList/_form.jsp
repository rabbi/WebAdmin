<%@ page language="java" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="app" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<form:hidden path="id"></form:hidden>
<c:forEach var="vo" items="${simpleForm}">

    <c:if test="${vo.formType eq 'input'}">
        <app:s_input labelName="${vo.formName}" path="${vo.formKey}"></app:s_input>
    </c:if>
</c:forEach>

