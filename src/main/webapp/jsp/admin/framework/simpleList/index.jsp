<%@ page language="java" pageEncoding="utf-8" %>
<%@ include file="../../header.jsp" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="app" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<section class="content">
    <app:t_message></app:t_message>

    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">${menuTitle}</h3>
        </div>
        <form class="form-horizontal">
            <div class="box-body">
                <app:hiddenInput id="menuid" value="${param.menuid}"></app:hiddenInput>
                <app:hiddenInput id="key" value="${param.key}"></app:hiddenInput>
                <c:forEach var="vo" items="${tableFilter}">
                    <c:if test="${vo.filterType eq 'text'}">
                        <app:searchinput defaultValue="${param[vo.filterKey]}" labelName="${vo.filterName}"
                                         inputID="${vo.filterKey}"></app:searchinput>
                    </c:if>
                </c:forEach>
            </div>
            <div class="box-footer">
                <sec:authorize access="hasAuthority('${config.authprefix}_ADD')">
                    <app:addButton></app:addButton>
                </sec:authorize>
                <app:searchButton></app:searchButton>
            </div>
        </form>
    </div>
    <div class="box">
        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
            <table class="table table-hover">
                <tbody>
                <tr>
                    <c:forEach var="vo" items="${tableTitles}">
                        <th style="width: 10%;">${vo}</th>
                    </c:forEach>
                    <th style="width: 15%;">操作</th>
                </tr>
                <c:forEach var="vo" items="${result}">
                    <tr>
                        <c:forEach var="so" items="${tableFieldsList}">
                            <td>${vo[so]}</td>
                        </c:forEach>
                        <td>
                            <sec:authorize access="hasAuthority('${config.authprefix}_EDIT')">
                                <app:editButton id="${vo.id}"></app:editButton>
                            </sec:authorize>
                            <sec:authorize access="hasAuthority('${config.authprefix}_DELETE')">
                                <app:deleteButton id="${vo.id}"></app:deleteButton>
                            </sec:authorize>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>

            </table>
        </div>
        <%@include file="../../common/pagenation.jsp" %>
    </div>

</section>

<%@ include file="../../footer.jsp" %>