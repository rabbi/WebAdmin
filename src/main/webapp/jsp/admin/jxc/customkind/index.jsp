<%@ page language="java" pageEncoding="utf-8" %>
<%@ include file="../../header.jsp" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="app" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<section class="content">
    <app:t_message></app:t_message>

    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">客户类别管理</h3>
        </div>
        <form class="form-horizontal">
            <div class="box-body">
                <p></p>
            </div>
            <div class="box-footer">
                <sec:authorize access="hasAuthority('ROLE_JXC_CUSTOMKIND_ADD')">
                    <app:addButton></app:addButton>
                </sec:authorize>
                <app:searchButton></app:searchButton>
            </div>
        </form>
    </div>
    <div class="box">
        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
            <table class="table table-hover">
                <tbody>
                <tr>
                    <th style="width: 10%;">客户类别名称</th>
                    <th style="width: 10%;">创建人</th>
                    <th style="width: 10%;">创建时间</th>
                    <th style="width: 10%;">更新人</th>
                    <th style="width: 10%;">更新时间</th>
                    <th style="width: 15%;">操作</th>
                </tr>
                <c:forEach var="vo" items="${result}">
                    <tr>
                        <td>${vo.name}</td>
                        <td>${vo.creator}</td>
                        <td>${vo.createtime}</td>
                        <td>${vo.updator}</td>
                        <td>${vo.updatetime}</td>
                        <td>
                            <sec:authorize access="hasAuthority('ROLE_JXC_CUSTOMKIND_EDIT')">
                                <app:editButton id="${vo.id}"></app:editButton>
                            </sec:authorize>
                            <sec:authorize access="hasAuthority('ROLE_JXC_CUSTOMKIND_DELETE')">
                                <app:deleteButton id="${vo.id}"></app:deleteButton>
                            </sec:authorize>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>

            </table>
        </div>
        <%@include file="../../common/pagenation.jsp" %>
    </div>

</section>

<%@ include file="../../footer.jsp" %>