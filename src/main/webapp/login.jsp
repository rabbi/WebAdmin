<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>WebAdmin | 登陆</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet"
          href="${pageContext.servletContext.contextPath}/static/adminlte/bootstrap/css/bootstrap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="${pageContext.servletContext.contextPath}/static/adminlte/dist/css/AdminLTE.min.css">
    <!-- iCheck -->
    <link rel="stylesheet"
          href="${pageContext.servletContext.contextPath}/static/adminlte/plugins/iCheck/square/blue.css">

</head>
<body class="hold-transition login-page">
<div class="login-box">
    <div class="login-logo">
        <a href="../../index2.html"><b>Web</b>Admin</a>
    </div><!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg">登陆</p>
        <form action="${pageContext.servletContext.contextPath}/login" method="post">
            <c:if test="${param.maxnum != null}">
                <p>
                    该账户已登录
                </p>
            </c:if>
            <c:if test="${param.session != null}">
                <p>
                    当前会话已超时
                </p>
            </c:if>
            <c:if test="${param.error != null}">
                <p>
                    用户名密码不匹配
                </p>
            </c:if>
            <c:if test="${param.logout != null}">
                <p>
                    成功登出
                </p>
            </c:if>
            <div class="form-group has-feedback">
                <input type="text" name="username" class="form-control" placeholder="用户名">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input name="password" type="password" class="form-control" placeholder="密码">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>

            <input type="hidden"
                   name="${_csrf.parameterName}"
                   value="${_csrf.token}"/>

            <div class="row">
                <div class="col-xs-8">
                </div><!-- /.col -->
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">登陆</button>
                </div><!-- /.col -->
            </div>
        </form>

    </div><!-- /.login-box-body -->
</div><!-- /.login-box -->

<!-- jQuery 2.1.4 -->
<script src="${pageContext.servletContext.contextPath}/static/adminlte/plugins/jQuery/jQuery-2.1.4.min.js"></script>
<!-- Bootstrap 3.3.5 -->
<script src="${pageContext.servletContext.contextPath}/static/adminlte/bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="${pageContext.servletContext.contextPath}/static/adminlte/plugins/iCheck/icheck.min.js"></script>
</body>
</html>
