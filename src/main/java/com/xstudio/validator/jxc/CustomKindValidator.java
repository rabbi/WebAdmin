package com.xstudio.validator.jxc;

import com.xstudio.models.jxc.JxcCustomKind;
import com.xstudio.models.sys.SysMenu;
import com.xstudio.utilities.ValidationUtility;
import org.springframework.stereotype.Repository;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * Created by kira on 16-6-17.
 */
@Repository
public class CustomKindValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return true;
    }

    @Override
    public void validate(Object object, Errors errors) {
        JxcCustomKind jxcCustomKind = (JxcCustomKind) object;
        ValidationUtility.rejectBlank(jxcCustomKind.getName(), errors, "name", "jxc.name.required", "客户类型名称不可为空");
    }
}