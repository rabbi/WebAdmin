package com.xstudio.utilities;

import com.xstudio.mybatis.Pagination;
import com.xstudio.plugins.ControllerListPlugin;
import com.xstudio.plugins.IListPlugin;
import org.apache.ibatis.session.RowBounds;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.ui.Model;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by kira on 16-6-17.
 */
public class ControllerUtility {
    public static void buildSimpleList(HttpServletRequest request, Model model,
                                       IListPlugin iListPlugin, ControllerListPlugin controllerListPlugin) {
        try {
            Pagination pagination = CommonUtility.getPagination(request, model);
            HashMap<String, Object> map = CommonUtility.getParameterMap(request);

            CommonUtility.renderGridData(request, model,
                    iListPlugin.all(pagination, map),
                    pagination);
            if(controllerListPlugin!=null) {
                controllerListPlugin.init(request, model);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}
