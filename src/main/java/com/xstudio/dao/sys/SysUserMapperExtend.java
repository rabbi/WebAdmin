package com.xstudio.dao.sys;

import com.xstudio.plugins.IListPlugin;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;

@Repository
public interface SysUserMapperExtend extends IListPlugin{
    List<HashMap<String,Object>> loadUserAuth(Integer id);
}