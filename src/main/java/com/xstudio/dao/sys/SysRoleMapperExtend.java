package com.xstudio.dao.sys;

import com.xstudio.plugins.IListPlugin;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public interface SysRoleMapperExtend extends IListPlugin{
    List<HashMap<String, Object>> loadRoleViaUser(Map<String, String> map);
}