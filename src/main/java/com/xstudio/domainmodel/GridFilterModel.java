package com.xstudio.domainmodel;

/**
 * Created by kira on 16/7/9.
 */
public class GridFilterModel {
    private String filterName;
    private String filterType;
    private String filterKey;

    public GridFilterModel(String filterKey,
                           String filterType,
                           String filterName) {
        this.filterKey = filterKey;
        this.filterName = filterName;
        this.filterType = filterType;
    }


    public String getFilterName() {
        return filterName;
    }

    public void setFilterName(String filterName) {
        this.filterName = filterName;
    }

    public String getFilterType() {
        return filterType;
    }

    public void setFilterType(String filterType) {
        this.filterType = filterType;
    }

    public String getFilterKey() {
        return filterKey;
    }

    public void setFilterKey(String filterKey) {
        this.filterKey = filterKey;
    }
}
