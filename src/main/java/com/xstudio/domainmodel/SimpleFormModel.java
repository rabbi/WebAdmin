package com.xstudio.domainmodel;

/**
 * Created by kira on 16/7/9.
 */
public class SimpleFormModel {
    private String formName;
    private String formType;
    private String formKey;

    public SimpleFormModel(String filterKey,
                           String filterName,
                           String filterType) {
        this.formKey = filterKey;
        this.formName = filterName;
        this.formType = filterType;
    }


    public String getFormName() {
        return formName;
    }

    public void setFormName(String formName) {
        this.formName = formName;
    }

    public String getFormType() {
        return formType;
    }

    public void setFormType(String formType) {
        this.formType = formType;
    }

    public String getFormKey() {
        return formKey;
    }

    public void setFormKey(String formKey) {
        this.formKey = formKey;
    }
}
