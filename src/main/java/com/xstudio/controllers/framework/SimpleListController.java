package com.xstudio.controllers.framework;

import com.xstudio.dao.sys.SysConfigMapper;
import com.xstudio.dao.sys.SysMenuMapper;
import com.xstudio.domainmodel.GridFilterModel;
import com.xstudio.models.sys.SysConfig;
import com.xstudio.models.sys.SysConfigExample;
import com.xstudio.models.sys.SysMenu;
import com.xstudio.mybatis.Pagination;
import com.xstudio.plugins.IListPlugin;
import com.xstudio.utilities.CommonUtility;
import com.xstudio.utilities.SpringContainerUtility;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by kira on 16-6-19.
 */
@Controller
@RequestMapping(value = "/admin/framework/simplelist")
public class SimpleListController extends BaseGridController {
    String prefix = "admin/framework/simpleList/";

    @Resource
    SysConfigMapper sysConfigMapper;

    @Resource
    SysMenuMapper sysMenuMapper;

    @RequestMapping(value = "/index")
    public String index(HttpServletRequest request, Model model,
                        String key, Integer menuid) throws ClassNotFoundException {
        //设置菜单标题
        SysMenu menu = sysMenuMapper.selectByPrimaryKey(menuid);
        model.addAttribute("menuTitle", menu.getName());

        SysConfig entity = loadSysConfig(sysConfigMapper, key);
        model.addAttribute("config", entity);
        loadTableData(request, model, entity);

        buildTableInfo(model, entity);
        buildGridFilter(model, entity);
        return prefix + "index";
    }

    @RequestMapping(value = "/add")
    public String add(HttpServletRequest request, Model model,
                      String key,String menuName) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        SysConfig entity = loadSysConfig(sysConfigMapper, key);
        model.addAttribute("config", entity);
        buildFormFilter(model, entity);
        model.addAttribute("entity",Class.forName(entity.getModelname()).newInstance());
        return prefix + "add";
    }
}
