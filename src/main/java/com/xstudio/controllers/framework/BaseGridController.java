package com.xstudio.controllers.framework;

import com.xstudio.dao.sys.SysConfigMapper;
import com.xstudio.domainmodel.GridFilterModel;
import com.xstudio.domainmodel.SimpleFormModel;
import com.xstudio.models.sys.SysConfig;
import com.xstudio.models.sys.SysConfigExample;
import com.xstudio.mybatis.Pagination;
import com.xstudio.plugins.IListPlugin;
import com.xstudio.utilities.CommonUtility;
import com.xstudio.utilities.SpringContainerUtility;
import org.springframework.ui.Model;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by kira on 16/7/9.
 */
public class BaseGridController {

    SysConfig loadSysConfig(SysConfigMapper sysConfigMapper, String key) {
        SysConfigExample sysConfig = new SysConfigExample();
        sysConfig.createCriteria().andKeynameEqualTo(key);
        SysConfig entity = sysConfigMapper.selectByExample(sysConfig).get(0);
        return entity;
    }

    /**
     * 加载表格的过滤条件信息
     *
     * @param entity
     * @return
     */
    void buildGridFilter(Model model, SysConfig entity) {
        List<GridFilterModel> gridFilterModels = new ArrayList<>();
        String[] gridFilters = entity.getGridfilters().split(",");
        for (String gridFilter : gridFilters) {
            String[] filterInfo = gridFilter.split(":");
            GridFilterModel gridFilterModel = new GridFilterModel(
                    filterInfo[0],
                    filterInfo[1],
                    filterInfo[2]
            );
            gridFilterModels.add(gridFilterModel);
        }
        model.addAttribute("tableFilter", gridFilterModels);
    }

    void buildFormFilter(Model model, SysConfig entity) {
        List<SimpleFormModel> simpleFormModels = new ArrayList<>();
        String[] simpleFormModel = entity.getFormcontent().split(",");
        for (String form : simpleFormModel) {
            String[] formInfo = form.split(":");
            SimpleFormModel obj = new SimpleFormModel(
                    formInfo[0],
                    formInfo[1],
                    formInfo[2]
            );
            simpleFormModels.add(obj);
        }
        model.addAttribute("simpleForm", simpleFormModels);
    }

    /**
     * 加载表格信息
     *
     * @param model
     * @param entity
     */
    void buildTableInfo(Model model, SysConfig entity) {
        List<String> tableTitles = new ArrayList<>();
        String[] fields = entity.getFieldnames().split(",");
        for (String field : fields) {
            tableTitles.add(field);
        }

        List<String> tableFields = new ArrayList<>();
        String[] tableFieldsList = entity.getFields().split(",");
        for (String field : tableFieldsList) {
            tableFields.add(field);
        }
        model.addAttribute("tableFieldsList", tableFieldsList);
        model.addAttribute("tableTitles", tableTitles);
        model.addAttribute("tableLength", tableTitles.size());
    }

    void loadTableData(HttpServletRequest request, Model model, SysConfig entity) {
        IListPlugin extendMapper = null;
        try {
            extendMapper = (IListPlugin) SpringContainerUtility.context.getBean(Class.forName(entity.getMappername() + "Extend"));
            Pagination pagination = CommonUtility.getPagination(request, model);
            HashMap<String, Object> map = CommonUtility.getParameterMap(request);

            List<HashMap<String, Object>> result = extendMapper.all(pagination, map);
            CommonUtility.renderGridData(request, model,
                    result,
                    pagination);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }
}
