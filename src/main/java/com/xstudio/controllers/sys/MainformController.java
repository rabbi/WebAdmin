package com.xstudio.controllers.sys;

import org.hyperic.sigar.SigarException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by kira on 16/2/28.
 */
@Controller
@RequestMapping(value = "/admin")
public class MainformController {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @PreAuthorize("hasAuthority('ROLE_SYS_MAINFORM')")
    @RequestMapping(value = "/index")
    public String index(HttpServletRequest request, Model model) throws SigarException {
        Runtime runtime = Runtime.getRuntime();
        model.addAttribute("availableProcessors", runtime.availableProcessors());

        return "admin/index";
    }

    @RequestMapping(value = "/setMenuSession")
    @ResponseBody
    public String setMenuSession(HttpServletRequest request, Model model,
                                 String id, String subid) {
        request.getSession().setAttribute("_sid", id);
        request.getSession().setAttribute("_subid", subid);
        return "";
    }
}
