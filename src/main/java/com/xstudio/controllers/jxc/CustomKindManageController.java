package com.xstudio.controllers.jxc;

import com.xstudio.dao.jxc.JxcCustomKindMapper;
import com.xstudio.dao.jxc.JxcCustomKindMapperExtender;
import com.xstudio.dao.sys.SysDictMapper;
import com.xstudio.dao.sys.SysDictMapperExtend;
import com.xstudio.models.jxc.JxcCustomKind;
import com.xstudio.models.sys.SysDict;
import com.xstudio.utilities.CommonUtility;
import com.xstudio.utilities.ControllerUtility;
import com.xstudio.validator.jxc.CustomKindValidator;
import com.xstudio.validator.sys.SysDictValidator;
import org.apache.log4j.Logger;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

/**
 * 客户类别管理
 */
@Controller
@RequestMapping(value = "/admin/jxc/customkind")
public class CustomKindManageController {
    private Logger logger = Logger.getLogger(CustomKindManageController.class);

    String prefix = "admin/jxc/customkind/";

    @Resource
    CustomKindValidator customKindValidator;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.setValidator(customKindValidator);
    }

    @Resource
    JxcCustomKindMapper jxcCustomKindMapper;

    @Resource
    JxcCustomKindMapperExtender jxcCustomKindMapperExtender;


    @PreAuthorize("hasAuthority('ROLE_JXC_CUSTOMKIND_LIST')")
    @RequestMapping(value = "/index")
    public String index(HttpServletRequest request, Model model) {
        ControllerUtility.buildSimpleList(request, model, jxcCustomKindMapperExtender, null);
        return prefix + "index";
    }


    @PreAuthorize("hasAuthority('ROLE_JXC_CUSTOMKIND_ADD')")
    @RequestMapping(value = "/add")
    public String add(HttpServletRequest request, Model model,
                      @ModelAttribute("jxcCustomKind") JxcCustomKind jxcCustomKind, BindingResult result) {
        return prefix + "add";
    }


    @PreAuthorize("hasAuthority('ROLE_JXC_CUSTOMKIND_ADD')")
    @RequestMapping(value = "/addEntity")
    public ModelAndView addEntity(HttpServletRequest request, Model model,
                                  @Valid @ModelAttribute("jxcCustomKind") JxcCustomKind jxcCustomKind,
                                  BindingResult bindingResult,
                                  RedirectAttributes redirectAttributes) {
        return CommonUtility.commonAdd(bindingResult, prefix, jxcCustomKind, "新增客户类型", jxcCustomKindMapper,
                redirectAttributes, logger);
    }


    @PreAuthorize("hasAuthority('ROLE_JXC_CUSTOMKIND_EDIT')")
    @RequestMapping(value = "/edit")
    public String edit(HttpServletRequest request, Model model, Integer id) {
        model.addAttribute("jxcCustomKind", jxcCustomKindMapper.selectByPrimaryKey(id));
        return prefix + "edit";
    }


    @PreAuthorize("hasAuthority('ROLE_JXC_CUSTOMKIND_EDIT')")
    @RequestMapping(value = "/editEntity")
    public ModelAndView editEntity(HttpServletRequest request, Model model,
                                   @Valid @ModelAttribute("jxcCustomKind") JxcCustomKind jxcCustomKind,
                                   BindingResult bindingResult,
                                   RedirectAttributes redirectAttributes) {
        return CommonUtility.commonEdit(bindingResult, prefix, jxcCustomKind, "编辑客户类型", jxcCustomKindMapper, redirectAttributes,
                logger);
    }


    @PreAuthorize("hasAuthority('ROLE_JXC_CUSTOMKIND_DELETE')")
    @RequestMapping(value = "/deleteEntity")
    @ResponseBody
    public ModelAndView deleteEntity(HttpServletRequest request, Model model,
                                     Integer id, String limit, String offset,
                                     RedirectAttributes redirectAttributes) {
        return CommonUtility.commonDelete(prefix, "删除客户类型", jxcCustomKindMapper, id, redirectAttributes, limit,
                offset, logger);
    }
}
