package com.xstudio.plugins;

import com.xstudio.mybatis.Pagination;

import java.util.HashMap;
import java.util.List;

/**
 * Created by kira on 16-6-17.
 */
public interface IListPlugin {
    List<HashMap<String, Object>> all(Pagination pagination,
                                      HashMap<String, Object> map);
}
