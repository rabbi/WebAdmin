package com.xstudio.plugins;

import org.springframework.ui.Model;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by kira on 16-6-17.
 */
public interface ControllerListPlugin {
    void init(HttpServletRequest httpServletRequest, Model model);
}
